package com.example.homework28.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class ShoppingCart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(mappedBy = "shoppingCart")
   // @JoinColumn(name = "product_id")
    private List<Product> products;

    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
}
