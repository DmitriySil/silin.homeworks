package com.example.homework28.forms;

import lombok.Data;

@Data
public class CustomerForm {

    private String firstName;
    private String lastName;

}
