package com.example.homework28.forms;

import lombok.Data;

@Data
public class ProductForm {

    private Integer id;
    private String title;
    private String description;
    private String descriptionProduct;
    private Integer price;
    private Integer amount;
    private Integer amountInCart;
}
