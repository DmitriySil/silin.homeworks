package com.example.homework28.service;

import com.example.homework28.dto.CustomerDto;
import com.example.homework28.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final CustomerRepository customerRepository;

    @Override
    public CustomerDto getUser(Integer currentUserId) {
        return CustomerDto.from(customerRepository.getById(currentUserId));
    }
}
