package com.example.homework28.service;

import com.example.homework28.forms.ProductForm;
import com.example.homework28.models.Product;

import java.util.List;

public interface ProductsService {

    void addProduct(ProductForm form);
    List<Product> getAllProducts();

    void deleteProduct(Integer id);

    Product getProduct(Integer id);

    void updateProduct(ProductForm form, Integer id);

    void deleteProductFromShoppingCart(Integer productId);
}
