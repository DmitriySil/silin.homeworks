package com.example.homework28.service;

import com.example.homework28.models.Product;
import com.example.homework28.models.ShoppingCart;

import java.util.List;

public interface ShoppingCartService {
    void addProductToCart(Integer productId, Integer cartId);


    List<Product> getAllProducts(Integer cartId);

    void deleteProductFromShoppingCart(Integer productId);

    ShoppingCart getCurrentCartById(Integer cartId);
}
