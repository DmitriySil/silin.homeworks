package com.example.homework28.service;

import com.example.homework28.models.Customer;
import com.example.homework28.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;


    @Override
    public Customer getCustomerByEmail(String customerEmail) {
        return customerRepository.findCustomersByEmail(customerEmail);
    }

    @Override
    public Optional<Customer> getUser(Integer currentUserId) {
        return customerRepository.getCustomerById(currentUserId);
    }
}
