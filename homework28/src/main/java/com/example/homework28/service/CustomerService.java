package com.example.homework28.service;

import com.example.homework28.models.Customer;

import java.util.Optional;

public interface CustomerService {
    Customer getCustomerByEmail(String customerEmail);

    Optional<Customer> getUser(Integer currentUserId);
}
