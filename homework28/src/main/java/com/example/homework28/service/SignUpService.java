package com.example.homework28.service;

import com.example.homework28.forms.SignUpForm;

public interface SignUpService {
    void signUpCustomer(SignUpForm form);
}
