package com.example.homework28.service;

import com.example.homework28.dto.CustomerDto;

public interface ProfileService {
    CustomerDto getUser(Integer currentUserId);
}
