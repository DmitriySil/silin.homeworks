package com.example.homework28.service;

import com.example.homework28.models.Product;
import com.example.homework28.models.ShoppingCart;
import com.example.homework28.repositories.ProductsRepository;
import com.example.homework28.repositories.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductsRepository productsRepository;


    @Autowired
    public ShoppingCartServiceImpl(ShoppingCartRepository shoppingCartRepository, ProductsRepository productsRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.productsRepository = productsRepository;
    }

    @Override
    public void addProductToCart(Integer productId, Integer cartId) {
        //TODO
        Product product = productsRepository.getById(productId);
        ShoppingCart cart = shoppingCartRepository.getById(cartId);
        if (product.getAmount()>=1){
            if (product.getAmount()==1){
                product.setAmount(product.getAmount() -1);
                product.setAmountInCart(product.getAmountInCart() + 1);
                product.setDescription("Нет в наличии");
                productsRepository.save(product);
                return;
            }
            product.setAmount(product.getAmount() -1);
            product.setAmountInCart(product.getAmountInCart() + 1);
            if (product.getAmountInCart()==1){
                product.setShoppingCart(cart);
            }
            shoppingCartRepository.save(cart);
        }
    }

    @Override
    public List<Product> getAllProducts(Integer cartId) {
        return productsRepository.findProductByShoppingCart_Id(cartId);
    }

    @Override
    public void deleteProductFromShoppingCart(Integer productId) {
        Product product = productsRepository.getById(productId);
        if (product.getAmountInCart()>=1) {
            if (product.getAmountInCart()==1){
                product.setAmountInCart(product.getAmountInCart()-1);
                product.setAmount(product.getAmount()+1);
                product.setDescription(product.getDescriptionProduct());
                product.setShoppingCart(null);
                productsRepository.save(product);
                return;
            }
            product.setAmountInCart(product.getAmountInCart()-1);
            product.setAmount(product.getAmount()+1);
            if (product.getAmount()>0){
                product.setDescription(product.getDescriptionProduct());
            }
            productsRepository.save(product);
        }
    }

    @Override
    public ShoppingCart getCurrentCartById(Integer cartId) {
        return shoppingCartRepository.getById(cartId);
    }
}
