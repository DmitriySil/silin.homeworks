package com.example.homework28.service;

import com.example.homework28.forms.ProductForm;
import com.example.homework28.models.Product;
import com.example.homework28.repositories.ProductsRepository;
//import com.example.homework28.repositories.jdbc.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductsServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .id(form.getId())
                .title(form.getTitle())
                .description(form.getDescription())
                .descriptionProduct(form.getDescription())
                .price(form.getPrice())
                .amount(form.getAmount())
                .amountInCart(0)
        .build();
        productsRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public void deleteProduct(Integer id) {
        productsRepository.deleteById(id);
    }

    @Override
    public Product getProduct(Integer id) {
        return productsRepository.getById(id);
    }

    @Override
    public void updateProduct(ProductForm form, Integer id) {
        Product product = Product.builder()
                .id(id)
                .title(form.getTitle())
                .description(form.getDescription())
                .descriptionProduct(form.getDescription())
                .price(form.getPrice())
                .amount(form.getAmount())
                .amountInCart(0)
                .build();
        productsRepository.save(product);
    }

    @Override
    public void deleteProductFromShoppingCart(Integer productId) {
        Product product = productsRepository.findProductById(productId);
        product.setShoppingCart(null);

    }

}
