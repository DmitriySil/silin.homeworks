package com.example.homework28.service;

import com.example.homework28.forms.SignUpForm;
import com.example.homework28.models.Customer;
import com.example.homework28.models.ShoppingCart;
import com.example.homework28.repositories.CustomerRepository;
import com.example.homework28.repositories.ShoppingCartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final CustomerRepository customerRepository;
    private final ShoppingCartRepository shoppingCartRepository;

    @Override
    public void signUpCustomer(SignUpForm form) {
        ShoppingCart shoppingCart = ShoppingCart.builder().build();
        shoppingCartRepository.save(shoppingCart);

        Customer customer = Customer.builder()
                .role(Customer.Role.CUSTOMER)
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .shoppingCart(shoppingCart)
                .password(passwordEncoder.encode(form.getPassword()))
                .build();

        shoppingCart.setCustomer(customer);
        customerRepository.save(customer);
    }
}
