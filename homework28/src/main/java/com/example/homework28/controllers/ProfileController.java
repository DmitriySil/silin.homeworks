package com.example.homework28.controllers;

import com.example.homework28.models.Customer;
import com.example.homework28.service.CustomerService;
import com.example.homework28.service.CustomerServiceImpl;
import com.example.homework28.service.ProfileService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class ProfileController {

    private final CustomerService customerService;;

    public ProfileController(CustomerService customerService) {
        this.customerService = customerService;
    }
}
