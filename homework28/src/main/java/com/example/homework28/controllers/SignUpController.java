package com.example.homework28.controllers;

import com.example.homework28.forms.SignUpForm;
import com.example.homework28.service.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage() {
        return "signUp";
    }

    @PostMapping
    public String signUpCustomer(SignUpForm form) {
        signUpService.signUpCustomer(form);
        return "redirect:/signIn";
    }
}
