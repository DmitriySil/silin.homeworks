package com.example.homework28.controllers;

import com.example.homework28.forms.ProductForm;
import com.example.homework28.models.Product;
import com.example.homework28.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class ProductController {

    private final ProductsService productsService;

    @Autowired
    public ProductController(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping("/products")
    public String getProductsPage(Model model) {
        List<Product> product = productsService.getAllProducts();
        model.addAttribute("products", product);
        return "products";
    }

    @PostMapping("/products")
    public String addProduct(ProductForm productForm) {

        productsService.addProduct(productForm);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer id) {
        productsService.deleteProduct(id);
        return "redirect:/products";
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Integer id) {
        Product product = productsService.getProduct(id);
        model.addAttribute("product", product);
        return "product";
    }

    @PostMapping("/products/{product-id}/update")
    public String updateProduct(ProductForm productForm, @PathVariable("product-id") Integer id) {
        productsService.updateProduct(productForm, id);
        return "redirect:/products";
    }
}
