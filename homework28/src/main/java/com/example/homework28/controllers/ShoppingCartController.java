package com.example.homework28.controllers;

import com.example.homework28.models.Customer;
import com.example.homework28.models.Product;
import com.example.homework28.models.ShoppingCart;
import com.example.homework28.service.CustomerService;
import com.example.homework28.service.ProductsService;
import com.example.homework28.service.ShoppingCartService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;
import java.util.List;

@Controller
public class ShoppingCartController {

    private final ProductsService productsService;
    private final ShoppingCartService shoppingCartService;
    private final CustomerService customerService;


    public ShoppingCartController(ProductsService productsService, ShoppingCartService shoppingCartService, CustomerService customerService) {
        this.productsService = productsService;
        this.shoppingCartService = shoppingCartService;
        this.customerService = customerService;
    }

    @GetMapping("/productsForSail")
    public String getProductsPage(Model model, Principal principal) {
        Customer customer = customerService.getCustomerByEmail(principal.getName());
        model.addAttribute("customer", customer);
        List<Product> product = productsService.getAllProducts();
        model.addAttribute("products", product);
        return "productsForSail";
    }
        //todo надо передать пользователя и корзину в методы!!
    @PostMapping("/shoppingCart/{product-id}/{cart-id}/add")
    public String addProductToCart(@PathVariable("product-id") Integer productId, @PathVariable("cart-id") Integer cartId) {
        shoppingCartService.addProductToCart(productId, cartId);
        return "redirect:/productsForSail";
    }

    @GetMapping("/shoppingCart/{Cart-id}")
    public String getShoppingCart(Model model, @PathVariable("Cart-id") Integer cartId) {//
        ShoppingCart cart = shoppingCartService.getCurrentCartById(cartId);
        model.addAttribute("shoppingCart", cart);
        List<Product> products = shoppingCartService.getAllProducts(cartId);
        model.addAttribute("products", products);
        return "shoppingCart";
    }

    @PostMapping("/shoppingCart/{product-id}/delete")
    public String deleteProductFromShoppingCart(@PathVariable("product-id") Integer productId) {
        shoppingCartService.deleteProductFromShoppingCart(productId);
        return "redirect:/shoppingCart/" + 6;
    }

}
