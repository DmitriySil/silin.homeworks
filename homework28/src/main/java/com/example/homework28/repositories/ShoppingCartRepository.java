package com.example.homework28.repositories;

import com.example.homework28.models.Product;
import com.example.homework28.models.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Integer> {
   // List<Product> addProduct(Product product);
}
