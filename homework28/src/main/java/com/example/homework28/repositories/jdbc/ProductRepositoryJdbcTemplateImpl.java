/*
package com.example.homework28.repositories.jdbc;

import com.example.homework28.forms.ProductForm;
import com.example.homework28.models.Product;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductRepositoryJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into product(title, description, price, amount) values(?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";

    //language=SQL
    private static final String SELECT_FROM_PRODUCT_WHERE_ID = "SELECT * FROM product WHERE id =?";

    //language=SQL
    private static final String SQL_DELETE = "DELETE FROM product WHERE id =?";

    //language=SQL
    private static final String SQL_UPDATE = "UPDATE product SET title = ?, description = ?, price = ?, amount = ? WHERE id = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from product where price = ?";

    private final JdbcTemplate jdbcTemplate;

    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        Long id = row.getLong("id");
        String title = row.getString("title");
        String description = row.getString("description");
        int price = row.getInt("price");
        int amount = row.getInt("amount");

        return new Product(id, title, description, price, amount);
    };


    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(Integer price) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, productRowMapper, price);

    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return null;
    }

    @Override
    public Product findById(Integer id) {
        return jdbcTemplate.queryForObject(SELECT_FROM_PRODUCT_WHERE_ID, productRowMapper, id);
    }

    @Override
    public void updateProduct(ProductForm productForm, Integer id) {
        jdbcTemplate.update(SQL_UPDATE, productForm.getTitle(), productForm.getDescription(),
                productForm.getPrice(), productForm.getAmount(), id);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getTitle(), product.getDescription(), product.getPrice(), product.getAmount());
    }

    @Override
    public void deleteById(Integer id) {
        jdbcTemplate.update(SQL_DELETE, id);

    }

}
*/
