package com.example.homework28.repositories;

import com.example.homework28.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductsRepository extends JpaRepository<Product, Integer> {

    List<Product> findProductByShoppingCart_Id(Integer cartId);

    Product findProductById(Integer id);
}
