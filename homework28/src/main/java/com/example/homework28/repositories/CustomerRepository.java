package com.example.homework28.repositories;

import com.example.homework28.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    Optional<Customer> findByEmail(String email);

    Customer findCustomersByEmail(String email);

    Optional<Customer> getCustomerById(Integer currentUserId);
}
