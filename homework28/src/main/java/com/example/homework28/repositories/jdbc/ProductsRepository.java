/*
package com.example.homework28.repositories.jdbc;

import com.example.homework28.forms.ProductForm;
import com.example.homework28.models.Product;

import java.util.List;

public interface ProductsRepository {


    void save(Product product);
    void deleteById(Integer id);
    List<Product> findAll();
    List<Product> findAllByPrice(Integer price);
    List<Product> findAllByOrdersCount(int ordersCount);

    Product findById(Integer id);

    void updateProduct(ProductForm productForm, Integer id);
}
*/
