package com.example.homework28.dto;

import com.example.homework28.models.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CustomerDto {
    private Integer id;
    private String email;
    private String firstName;
    private String lastName;

    public static CustomerDto from(Customer customer) {
        return CustomerDto.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .email(customer.getEmail())
                .build();
    }

    public static List<CustomerDto> from(List<Customer> customers) {
        return customers.stream().map(CustomerDto::from).collect(Collectors.toList());
    }
}
